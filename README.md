# Auto-Devops

## Somaire 
* [Description](#description)
* [Technologies](#technologies)
* [Setup](#setup)
* [Important](#important)
* [Pipelines-Environnement](#pipelines-environnement)
* [Pipelines-Application](#pipelines-application)



## Description
Ce projet à pour but d'économiser le temps passé sur la configuration des pipelines en mettant en place un pipeline multi-environnements fonctionnel, contrôlée par une application web. 

## Technologies
* Tearraform version: 0.14.6
* Angular version: 7.2.11
* Node.js version: 14.15.4
* Docker: version 20.10.2
* Gitlab CI/CD

## Setup
* Les 2 repertoires (Application et Environnement) doit être pusher sur 2 peojets gitlab differents.
* Pour assurer le bon fonctionnement des pipelines, vous devrais d'abord créer des variables Gitlab comme illustré sur le tableau ci-dessous dans le projet ou les pipelines vont se lancer.<br/>

| Key | Value | Repository |
| --- | ------------- | ---- |
| AWS_ACCESS_KEY_ID | Votre aws access key | Application et Environnement |
| AWS_ACCOUNT_ID | Votre ID composé de 12 chiffres sur aws | Application et Environnement |
| AWS_DEFAULT_REGION | La region où vous souhaitez déloyer votre infrastructure | Application et Environnement |
| AWS_SECRET_ACCESS_KEY | Votre aws secret key | Application et Environnement |
| UNIQUE | Une chaine de caractére tout en minuscule afin de donner un nom unique pour votre S3 bucket | Application et Environnement |
| API_URL_nomDeVotreEnv | L'url vers votre API, sous la forme http://dns-load-balancer-de-votre-env:4000. Cette varibale doit être créer pour chaque environnement | Application |
| CERT_PATH_nomDeVotreEnv | Le path vers le repertoire qui a la certificat ssl de la base de données documentDb. Par exepmle `./prod-cert/rds-combined-ca-bundle.pem`. Cette varibale doit être créer pour chaque environnement | Application |
| DB_URL_nomDeVotreEnv | L'url vers la base de données, qui se trouve dans le cluster documentDb, le mot de passe est testtest. Cette varibale doit être créer pour chaque environnement | Application |

* La syntaxe des noms des variables doit être obligatoirement comme indiqué sur le tableau.
* Les variables doivent d'être ni masquées ni protigées.
* Le code de front de votre application doit être dans le repertoire `application/front/`.
* Le code de front de votre application doit être dans le repertoire `application/back/`.

## Important
* Les branches dans les 2 repositories doivent avoir les mêmes noms.
* Dans le job 'plan' l'utilisateur peut savoir les ressources qui vont être déployées.
* L'utilisateur doit lancer le déploiement manuellement par mésure de sécurité.

#### A ajouter:
Afin de rassuré le bon fonctionemment des variables d'environnement, dans votre code Angular:
* Les deux fichiers (`front/env.js` et `front/env.template.js`) doivent être ajoutées dans `fornt/src/assets/`.
* Dans `src/environnement/environemment.ts`, ajoutez :
`apiUrl: window["env"]["apiUrl"] || "default",`</br>
  `debug: window["env"]["debug"] || false`
dans la classe environnement.
* Dans `src/index.html` ajoutez:
`<head>`</br>
`<script src="assets/env.js"></script>`</br>
`</head>`


## Pipelines-Environnement
Les piepelines déploient les ressources nécéssaires pour accueillir votre application (vpc, subnets, load balancers,...).

#### To-do:
* Les noms des branches sauf master doivent avoir le même nom que celui de l'environnement.
* Pour déployer un environmment, une branche avec le nom de l'environemment doit être créer.
* Sur master en cliquant sur le premier job, une autre pipeline responsable sur le déploiment des bucket S3 et des repositories ECR s'affiche. L'utilisateur doit lancer le dernier job manuellement 'deploy'. cette pipeline doit être lancer en premier.

## Pipelines-Application
#### Un seul environnement:
* Votre environnement est lié à la branche master.
* Le pipeline build les images, et les dépoloie sur le service ECR d'aws afin de les sauvrgader.
* le pipeline lance les conteneurs avec les images déployer sur le service ECS Fargate d'aws.
* Le tag de l'image est le tag créer.

#### To-do:
* Afin de délpoyer l'application sur ce environnement, la création d'un tag sous forme `v[0-99].[0-99].[0-99]_prod`, est nécéssaire.


#### Deux environnements:
##### `environnement 1`:
* Ce environnement est lié à la branche master.
* Les images Docker déployées par ce environnement sont la même image crée sur l'autre, et elles prennet le tag de git comme tag.

#### To-do:
* Afin de délpoyer l'application sur ce environnement, la création d'un tag sous forme `v[0-99].[0-99].[0-99]_prod`, est nécéssaire.

##### `environemment 2`:
* Le pipeline dans ce environnement fait le build des images Docker, et les dépoloie sur le service ECR d'aws afin de les sauvrgader.
* Les images Docker prennent les 8 premiers caractéres de l'id de commit comme tag.

#### To-do:
* Afin lancer le piepeline pour la premiére fois, la création d'une branche avec le même nom est obligatoire.
* Afin déployer l'application dans ce environnement, il suffit de pusher un commit.


#### Plus de deux environnements:
##### `environnement 1`:
* Ce environnement est lié à la branche master. 
* Les images Docker déployées par ce environnement sont les même images crée sur le 2 éme environnement entrée par l'utilisateur, et elles prennet le tag de git comme tag. 

#### To-do:
* Afin de délpoyer l'application sur ce environnement, la création d'un tag sous forme `v[0-99].[0-99].[0-99]_prod`, est nécéssaire.

##### `environemment 2`:
* Le pipeline build les images Docker, et les dépoloie sur le service ECR d'aws afin de les sauvrgader.
* Les images Docker prennent le nom de la brache sans aucun caratére spécial ajouté (les points sont permet) comme tag.
* Le pipeline a un job 'destroy' pour suprimmer les ressources deploiyées sur ce environnement.

#### To-do:
* Afin de délpoyer l'application sur ce environnement, une branche avec le même nom de l'environnement est obligatoire.

##### `Les autres environemments`:
* Le pipeline build les images Docker, et les dépoloie sur le service ECR d'aws afin de les sauvrgader.
* Les images Docker prennent les 8 premiers caractéres de l'id de commit comme tag.

#### To-do:
* Afin de lancer le piepelines pour la premiére fois, la création d'une branche avec le même nom est obligatoire.
* Afin de déployer l'application dans ces environnements, il suffit de pusher un commit.



